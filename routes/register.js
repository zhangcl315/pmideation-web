var express = require('express');
var router = express.Router();

/* GET register. */
router.get('/', function(req, res, next) {
    res.render('register', {
        pageTitle: 'Register!',
        formTitle: 'Register Form',
        formText: 'Register to our site'
    });
});

module.exports = router;
