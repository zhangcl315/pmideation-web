var express = require('express');
var router = express.Router();

/* GET index. */
router.get('/', function(req, res, next) {
    res.render('index', {
        pageTitle: 'Welcome!',
        formTitle: 'Login Form',
        formText: 'Login to our site'
    });
});

module.exports = router;
