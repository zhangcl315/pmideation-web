var express = require('express');
var router = express.Router();

/* GET success. */
router.get('/', function(req, res, next) {
    res.render('Success', {
        pageTitle: 'Success!',
        formText: 'Click button to return'
    });
});

module.exports = router;