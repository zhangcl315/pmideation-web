module.exports = function(grunt) {

    grunt.initConfig({
        // 读取package配置
        pkg: grunt.file.readJSON('package.json'),
        // js语法检查
        jshint: {
            files: ['Gruntfile.js', 'public/js/**/*.js'],
            options: {
                // 控制分号的警告
                asi: true,
                boss: true,
                // 控制url中return false的警告
                scripturl: true,
                // 绝对等于的警告
                eqeqeq: false,
                loopfunc: true,
                // 这个选项用于抑制表达式相关的一些错误信息
                expr: true,
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        // js压缩
        uglify: {
            options: {
                //不混淆变量名
                mangle: false,
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            build: {
                files:[
                    {
                        expand: true,
                        cwd: 'public',
                        src: 'public/js/**/*.js'
                    }
               ]
            }
        }
    });

    // 加载grunt插件
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    // 注册插件
    grunt.registerTask('default', ['jshint', 'uglify']);
};